package com.atlassian.plugin.spring.scanner.test;

import org.springframework.stereotype.Component;

@Component("namedComponent")
public class NamedComponent {
}
