package com.atlassian.plugin.spring.scanner.test.imported;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.InternalComponent;
import com.atlassian.plugin.spring.scanner.test.InternalComponentTwo;
import com.atlassian.plugin.spring.scanner.test.otherplugin.ServiceExportedFromAnotherPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@SuppressWarnings({"FieldCanBeLocal", "UnusedDeclaration"})
@Component("namedMixed")
public class NamedConsumingMixedComponents {
    private final ServiceExportedFromAnotherPlugin externalService;
    private final InternalComponent internalComponent;
    private final InternalComponentTwo internalComponentTwo;

    @Autowired
    public NamedConsumingMixedComponents(@ComponentImport final ServiceExportedFromAnotherPlugin externalService,
                                         final InternalComponent internalComponent,
                                         final InternalComponentTwo internalComponentTwo) {
        this.externalService = externalService;
        this.internalComponent = internalComponent;
        this.internalComponentTwo = internalComponentTwo;
    }
}
