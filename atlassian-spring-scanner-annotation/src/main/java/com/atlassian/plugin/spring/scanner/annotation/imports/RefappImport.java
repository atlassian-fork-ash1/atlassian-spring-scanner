package com.atlassian.plugin.spring.scanner.annotation.imports;

import com.atlassian.plugin.spring.scanner.ProductFilter;
import com.atlassian.plugin.spring.scanner.annotation.OnlyInProduct;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation representing an OSGi service that's required to be imported into this bundle
 * when running within Refapp. Can be applied to constructor params where the param type
 * is a service interface exported by another bundle
 */
@Target({ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@ComponentImport
@OnlyInProduct(ProductFilter.REFAPP)
public @interface RefappImport {

    /**
     * Override the default spring bean name to assign <b>within this plugin</b> for the imported OSGi service.
     * Not usually needed.
     *
     * @see ComponentImport#value() for more info.
     */
    String value() default "";
}
