package com.atlassian.plugin.spring.scanner.core;

import com.atlassian.plugin.spring.scanner.annotation.Profile;
import javassist.bytecode.ClassFile;
import org.slf4j.Logger;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import static java.util.Collections.emptySet;

/**
 * This little guy can help find the package-info.java of a given class file and cache what profiles are in play and
 * also what profiles are in a given class file.
 */
class ProfileFinder {
    private static final String PACKAGE_INFO = "package-info";
    private final Set<URL> buildPath;
    private final Logger log;
    private final JavassistHelper javassistHelper;
    private final Map<String, Set<String>> mapOfPackagesToProfiles = new TreeMap<String, Set<String>>();

    public ProfileFinder(final Set<URL> buildPath, final Logger log) {
        this.buildPath = buildPath;
        this.log = log;
        this.javassistHelper = new JavassistHelper();
    }

    boolean isPackageClass(ClassFile classFile) {
        return classFile.getName().endsWith("." + PACKAGE_INFO);
    }

    /**
     * Called to get the Profiles that apply to this class file
     *
     * @param classFile the class file in question
     * @return a set of applicable profiles
     */
    Set<String> getProfiles(ClassFile classFile) {
        String classFileName = classFile.getName();
        if (log.isDebugEnabled()) {
            log.debug(String.format("Checking for profile annotations on %s", classFileName));
        }

        if (isPackageClass(classFile)) {
            recordPackageClassProfileAnnotations(classFile);
            return emptySet();
        } else {
            Set<String> packageInfoProfileAnnotations = getPackageInfoProfileAnnotations(classFile);
            Set<String> classSpecificProfiles = findProfiles(classFile);
            //
            // if we have specific @Profile annotations on the class then they override the package level ones
            if (!classSpecificProfiles.isEmpty()) {
                return classSpecificProfiles;
            }
            return packageInfoProfileAnnotations;
        }
    }

    /**
     * Records in the cache just the package info class file profiles
     *
     * @param packageInfoClassFile should only be a package-info.class
     */
    private void recordPackageClassProfileAnnotations(final ClassFile packageInfoClassFile) {
        String packageName = getClassPackageAsFileName(packageInfoClassFile);
        Set<String> profiles = mapOfPackagesToProfiles.get(packageName);
        //
        // it possible that we have seen a package class member BEFORE we have seen the package-info class
        // and hence the other paths will have recorded the package-info profile information already
        //
        if (profiles == null) {
            mapOfPackagesToProfiles.put(packageName, findProfiles(packageInfoClassFile));
        }
    }

    private Set<String> getPackageInfoProfileAnnotations(final ClassFile classFile) {
        //
        // we need to look to see if we have seen this package before
        String packageName = getClassPackageAsFileName(classFile);
        Set<String> profiles = mapOfPackagesToProfiles.get(packageName);
        if (profiles != null) {
            return profiles;
        }
        //
        // ok we haven't see the package-info class yet.  So we do a preemptive lookup

        ClassFile packageInfoClassFile = loadPackageInfoClass(packageName);
        if (packageInfoClassFile != null) {
            recordPackageClassProfileAnnotations(packageInfoClassFile);
        } else {
            //
            // ok we have no package-info class so record that for posterity
            mapOfPackagesToProfiles.put(packageName, emptySet());
        }
        //
        // now lookup again cause it is now recorded
        return mapOfPackagesToProfiles.get(packageName);
    }

    private ClassFile loadPackageInfoClass(final String packageName) {
        for (URL url : buildPath) {
            try {
                File parentFile = new File(url.toURI());
                File packageInfoFile = new File(parentFile, packageName + "/" + "package-info.class");
                if (!packageInfoFile.exists()) {
                    continue;
                }

                try (InputStream inputStream = new FileInputStream(packageInfoFile)) {
                    DataInputStream dis = new DataInputStream(new BufferedInputStream(inputStream));
                    return new ClassFile(dis);
                } catch (IOException e) {
                    log.error(String.format("Could not create class file from %s", packageInfoFile), e);
                }
            } catch (URISyntaxException e) {
                log.error(String.format("Unable to load package-info file %s from %s", packageName, url));
            }
        }
        return null;
    }

    private Set<String> findProfiles(final ClassFile classFile) {
        List<String> classAnnotationNames = javassistHelper.getClassAnnotationNames(classFile);
        Set<String> profiles = new TreeSet<String>();
        for (String annotationType : classAnnotationNames) {
            if (Profile.class.getCanonicalName().equals(annotationType)) {
                Set<String> profileValues = javassistHelper.getAnnotationMemberSet(classFile, annotationType, "value");
                profiles.addAll(profileValues);
            }
            //
            // we have support for Spring 3.1 profile in a preemptive attempt that MAYBE one day we might get there :)
            if ("org.springframework.context.annotation.Profile".equals(annotationType)) {
                Set<String> profileValues = javassistHelper.getAnnotationMemberSet(classFile, annotationType, "value");
                profiles.addAll(profileValues);
            }
        }
        return profiles;
    }

    private String getClassPackageAsFileName(final ClassFile classFile) {
        String fileName = classFile.getName();
        int lastDot = fileName.lastIndexOf('.');
        if (lastDot == -1) {
            return fileName;
        }
        String packageName = fileName.substring(0, lastDot);
        return packageName.replace('.', '/');
    }
}
