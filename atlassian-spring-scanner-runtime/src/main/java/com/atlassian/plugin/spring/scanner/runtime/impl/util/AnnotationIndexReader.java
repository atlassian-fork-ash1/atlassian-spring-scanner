package com.atlassian.plugin.spring.scanner.runtime.impl.util;

import com.atlassian.plugin.spring.scanner.ProductFilter;
import com.atlassian.plugin.spring.scanner.util.CommonConstants;
import com.google.common.base.Charsets;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * A utility class to read index files from a classloader or bundle.
 * Can handle reading a single index file, or concating multiple index files based on the
 * currently running product.
 */
public class AnnotationIndexReader {
    private static final Logger log = LoggerFactory.getLogger(AnnotationIndexReader.class);

    /**
     * Read a file from a bundle and return the list of lines of the file.
     *
     * @param resourceFile the path to the file in the bundle
     * @param bundle       the bundle to read from
     * @return the lines in the file
     */
    public static List<String> readIndexFile(final String resourceFile, final Bundle bundle) {
        final URL url = bundle.getEntry(resourceFile);
        return readIndexFile(url);
    }

    /**
     * Read both the cross-product file and the product-specific file for the currently running product from the bundle and returns
     * their lines as a single list.
     *
     * @param resourceFile the path to the cross-product file in the bundle
     * @param bundle       the bundle to read from
     * @param bundleContext the bundle context used to determine the running product
     * @return the lines in the file
     */
    public static List<String> readAllIndexFilesForProduct(final String resourceFile, final Bundle bundle,
                                                           final BundleContext bundleContext) {

        final URL url = bundle.getEntry(resourceFile);

        final List<String> entries = new ArrayList<String>(readIndexFile(url));

        final ProductFilter filter = ProductFilterUtil.getFilterForCurrentProduct(bundleContext);
        if (null != filter) {
            entries.addAll(readIndexFile(filter.getPerProductFile(resourceFile), bundle));
        }

        return entries;
    }

    public static List<String> readIndexFile(final URL url) {
        final List<String> resources = new ArrayList<String>();

        try {
            if (null == url) {
                log.debug("Could not find annotation index file (null url).");
                return resources;
            }

            final BufferedReader reader;
            try {
                reader = new BufferedReader(new InputStreamReader(url.openStream(), Charsets.UTF_8));
            } catch (final FileNotFoundException e) {
                log.debug("Could not find annotation index file " + url);
                return resources;
            }

            String line = reader.readLine();
            while (line != null) {
                resources.add(line);

                line = reader.readLine();
            }

            if (log.isDebugEnabled()) {
                log.debug("Read annotation index file: " + url);
                log.debug("Printing out found annotated beans: ");
                log.debug(resources.toString());
            }

            reader.close();
        } catch (final IOException e) {
            throw new RuntimeException("Cannot read index file [" + url.toString() + "]", e);
        }

        return resources;
    }

    public static Properties readPropertiesFile(final URL url) {
        final Properties resources = new Properties();
        try {
            if (null == url) {
                return resources;
            }

            final BufferedReader reader;
            try {
                reader = new BufferedReader(new InputStreamReader(url.openStream(), Charsets.UTF_8));
            } catch (final FileNotFoundException e) {
                return resources;
            }

            resources.load(reader);
        } catch (final IOException e) {
            throw new RuntimeException("Cannot read properties file [" + url.toString() + "]", e);
        }
        return resources;
    }

    public static String[] splitProfiles(final String profiles) {
        return (profiles != null && !profiles.trim().isEmpty()) ? profiles.split(",") : new String[0];
    }

    public static Iterable<String> getIndexFilesForProfiles(final String[] profileNames, final String indexFileName) {
        final List<String> filesToRead = new ArrayList<String>();
        if (profileNames.length > 0) {
            for (String profileName : profileNames) {
                profileName = ((profileName == null) ? "" : profileName).trim();
                if (!profileName.isEmpty()) {
                    final String fileToRead = CommonConstants.INDEX_FILES_DIR + "/"
                            + CommonConstants.PROFILE_PREFIX + profileName + "/"
                            + indexFileName;

                    filesToRead.add(fileToRead);
                }
            }
        } else {
            final String fileToRead = CommonConstants.INDEX_FILES_DIR + "/" + indexFileName;
            filesToRead.add(fileToRead);
        }
        return filesToRead;
    }
}

