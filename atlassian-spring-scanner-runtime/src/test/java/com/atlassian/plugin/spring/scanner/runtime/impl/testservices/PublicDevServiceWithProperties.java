package com.atlassian.plugin.spring.scanner.runtime.impl.testservices;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.export.ServiceProperty;
import org.springframework.stereotype.Component;

@ExportAsService(
        value = {Service.class},
        properties = {
                @ServiceProperty(key = "dev_key_1", value = "dev_value_1"),
                @ServiceProperty(key = "dev_key_2", value = "dev_value_2")
        }
)
@Component
public class PublicDevServiceWithProperties implements Service {
    @Override
    public void a() {

    }
}
